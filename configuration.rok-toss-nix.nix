# https://nixos.org/guides/nix-pills/

{ config, pkgs, options, ... }:

{
  imports =
    [
      ./pkgs/dev.nix
      ./pkgs.nix
      ./hardware/rok-toss-nix.nix
    ];

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  nix.nixPath =
    # Prepend default nixPath values.
    options.nix.nixPath.default ++
    # Append our nixpkgs-overlays.
    [ "nixpkgs-overlays=./overlays/" ];

  nixpkgs.overlays = [
    (import (builtins.fetchTarball {
      url = https://github.com/nix-community/neovim-nightly-overlay/archive/master.tar.gz;
    }))
    # https://github.com/NixOS/nixpkgs/issues/244159
    (
      let
        pinnedPkgs = import
          (pkgs.fetchFromGitHub {
            owner = "NixOS";
            repo = "nixpkgs";
            rev = "b6bbc53029a31f788ffed9ea2d459f0bb0f0fbfc";
            sha256 = "sha256-JVFoTY3rs1uDHbh0llRb1BcTNx26fGSLSiPmjojT+KY=";
          })
          { };
      in
      final: prev: {
        docker = pinnedPkgs.docker;
      }
    )
  ];



  system.stateVersion = "23.05";
  nix.settings = {
    experimental-features = "nix-command flakes";
  };

  networking.hostName = "rok-toss-nix";
  networking.wireless.enable = false;

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.initrd.availableKernelModules = [ "virtiofs" ];

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Asia/Seoul";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  # TODO: put services together
  # services =
  #   {
  #     tailscale = {
  #       enable = true;
  #     };
  #
  #     xserver = {
  #       enable = true;
  #       displayManager = {
  #         sddm.enable = true;
  #       };
  #     };
  #   }

  services.tailscale.enable = true;

  # GUI
  services.xserver.enable = true;
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;

  systemd.services = {
    "fep_xxx" = {
      enable = true;
      path = [ ];
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      serviceConfig = {
        Type = "simple";
        User = "rok";
        Restart = "always";
        ExecStart = ''
          /home/rok/bin/go-slog
        '';
      };
    };
  };

  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };

  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;

  # Sound
  sound.enable = true;
  hardware.pulseaudio.enable = false;

  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # security
  #
  security.rtkit.enable = true;
  security.sudo.enable = true;
  security.sudo.extraRules = [
    {
      users = [ "rok" ];
      commands = [
        {
          command = "ALL";
          options = [ "NOPASSWD" ]; # "SETENV" # Adding the following could be a good idea
        }
      ];
    }
  ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.rok = {
    isNormalUser = true;
    description = "rok";
    extraGroups = [ "networkmanager" "wheel" "docker" ];
    packages = with pkgs; [
      firefox
      # pueue
      kate
      #  thunderbird
    ];
  };

  virtualisation.docker.enable = true;

  services.openssh.enable = true;
  services.qemuGuest.enable = true;
  services.spice-vdagentd.enable = true;
  services.syncthing = {
    enable = true;
    user = "rok";
    dataDir = "/home/rok"; # Default folder for new synced folders
    configDir = "/home/rok/.syncthing"; # Folder for Syncthing's settings and keys
  };

  networking.firewall.enable = false;

  fonts = {
    enableDefaultFonts = true;
    fonts = with pkgs; [
      ubuntu_font_family
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      liberation_ttf
      fira-code
      fira-code-symbols
      mplus-outline-fonts.githubRelease
      nerdfonts
      iosevka
      dina-font
      nanum
      proggyfonts
    ];

    fontconfig = {
      defaultFonts = {
        serif = [ "NanumGothic" "Noto Sans Mono" ];
        sansSerif = [ "NanumGothic" "Noto Sans Mono" ];
        monospace = [ "Noto Sans Mono" ];
      };
    };
  };

  environment.systemPackages = with pkgs; [
    _9pfs
    age
    aria2
    lnav
    atool
    bat
    bkt
    bolt
    chromium
    clang
    clang-tools_16
    convmv
    cron
    delta
    deno
    dig
    direnv
    docker
    dog
    dunst
    cloud-utils
    dura
    entr
    exa
    fish
    gh
    git-annex-utils
    glances
    glow
    unzip
    gnuplot
    vector
    gron
    helix
    helm
    hexyl
    jo
    just
    kitty
    ko
    krew
    kubectl
    kubectl-images
    kubectl-node-shell
    kubectl-tree
    kubectx
    kubetail
    libreoffice-qt
    lnav
    lshw
    mpv
    mupdf
    ncdu
    # neovim
    neovim-nightly
    netcat-gnu
    nginx
    nixpkgs-fmt
    zinc
    nmap
    nnn
    nodejs_20
    nodePackages_latest.pnpm
    okular
    ov
    p7zip
    pandoc
    phodav
    php82
    php82Packages.composer
    progress
    # pueue
    python3
    qemu
    rakudo
    ripgrep
    ruby
    socat
    sops
    spice-vdagent
    sqlite
    sshfs
    sshpass
    stern
    syncthing
    tailscale
    tcpdump
    termshark
    terraform
    texlive.combined.scheme-full
    tig
    tshark
    ttyd
    typst
    virtiofsd
    watchexec
    waypipe
    webkitgtk
    wev
    wireshark
    wl-clipboard
    xsel
    yarn
    zathura
    zef
    s3fs

    fuse3
    fuse-common
  ];

}
