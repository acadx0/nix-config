{ config, pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    visidata
    # oracle-instantclient
    bfs
    htop

    dura
    nushell
    recode
    caddy
    ipinfo

    awscli2
    # azure-cli
    # azure-storage-azcopy
    oci-cli

    zig
    # scrcpy

    gotests
    gopls
    gotestsum
    gomodifytags

    stylua
    lua
    # luajit
file

    # gcc
    gettext
    ninja
    lf
    meson
    pkg-config
    clang
    clang-tools_16
    # meilisearch
    gnumake
    cmake
    ncurses

    rustc
    cargo

    nodejs_20
    nodePackages_latest.node-gyp
    nodePackages_latest.pnpm
    nodePackages_latest.pyright
    nodePackages_latest.fx
    nodePackages_latest.prettier
    nodePackages.yaml-language-server
    nodePackages.vscode-langservers-extracted

    php82
    php82Packages.composer

    (python311.withPackages (p: with p; [
      regex
      pip
      requests
      setuptoolsBuildHook
      matplotlib
      numpy
      ptpython
      yt-dlp
      packaging
      pandas
    ]))
    pdm

    nixpkgs-fmt

    ruby

    deno

    go
    gofumpt
  ];
}
