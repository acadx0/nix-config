{ config, pkgs, ... }: {
  imports = [
    ./hardware/oci-xnzm1001-001.nix
  ];

  system.stateVersion = "23.05"; # Did you read the comment?

  networking.hostName = "oci-xnzm1001-001";
  networking.domain = "";

  services.tailscale.enable = true;

  services.openssh.enable = true;
  users.users.root.openssh.authorizedKeys.keys = [
    ''ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO/acNBaXuGBqtEyJoSMkrWXKYgQ/Q9c52SChgmh1ssT rok@rok-toss-nix''
    ''ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIC4PDiS3q4XfHGXd2om/ErP8kYr3dymD84XON3PTgBbM rok@rok-x1g10''
  ];

  zramSwap.enable = true;

  networking.firewall.enable = false;

  nixpkgs.config.permittedInsecurePackages = [ "nodejs-16.20.1" ];

  environment.systemPackages = with pkgs; [
    fzf
    git
    tailscale
    tmux
    jq
    gcc
    go
    fd
    github-runner
    inetutils
    aria2
    elvish
    vifm
    wget
    coreutils-full
    moreutils
    glibcLocales
    ghq
    stow
    gnumake
    entr
    procps
    vim
    zsh
    fish
    xsel
  ];
}
