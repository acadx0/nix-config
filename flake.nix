{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/master";

    # https://codeberg.org/totoroot/dotfiles/src/branch/main/flake.nix

    neovim-nightly-overlay = {
      url = "github:nix-community/neovim-nightly-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    home-manager = {
      url = github:nix-community/home-manager/release-23.05;
      inputs.nixpkgs.follows = "nixpkgs";
    };

    darwin = {
      url = "github:LnL7/nix-darwin";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    eza = {
      url = "github:eza-community/eza";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";

  };

  outputs = { self, nixpkgs, eza, nixpkgs-unstable, home-manager, agenix, darwin, ... }@inputs:
    let
    in
    {
      darwinConfigurations = {
        rok-toss =
          let username = "kyungrok.chung";
          in inputs.darwin.lib.darwinSystem rec {
            system = "aarch64-darwin";
            specialArgs = { inherit inputs; };
            modules = [
              ({ config, pkgs, ... }: { nixpkgs.overlays = [ inputs.neovim-nightly-overlay.overlay ]; })
              ./configuration.rok-toss.nix
              home-manager.darwinModules.home-manager
              {
                home-manager.useGlobalPkgs = true;
                home-manager.useUserPackages = true;
                home-manager.users."${username}" = import ./home.rok-toss.nix;
                users.users."${username}".home = "/Users/${username}";
              }
            ];
          };
      };

      nixosConfigurations = {
        rok-toss-nix = nixpkgs.lib.nixosSystem {
          system = "aarch64-linux";
          modules = [
            ({ config, pkgs, ... }: { nixpkgs.overlays = [ inputs.neovim-nightly-overlay.overlay ]; })
            ./configuration.rok-toss-nix.nix
            agenix.nixosModules.default
            {
              environment.systemPackages = [ agenix.packages.aarch64-linux.default eza.packages.aarch64-linux.default ];
            }
            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.rok = import ./home.rok-toss-nix.nix;
            }
          ];
        };

        root =
          let
            system = "x86_64-linux";
            overlay-unstable = final: prev: {
              unstable = import nixpkgs-unstable {
                inherit system;
                config.allowUnfree = true;
              };
            };

          in

          nixpkgs.lib.nixosSystem rec {
            modules = [
              ({ config, pkgs, ... }: { nixpkgs.overlays = [ inputs.neovim-nightly-overlay.overlay overlay-unstable ]; })

              agenix.nixosModules.default
              {
                environment.systemPackages = [ agenix.packages.x86_64-linux.default ];
              }
              home-manager.nixosModules.home-manager
              {
                home-manager.useGlobalPkgs = true;
                home-manager.useUserPackages = true;
                home-manager.users.rok = import ./home.root.nix;
              }

              ./configuration.root.nix
            ];
          };

        oci-xnzm1001-001 =
          let
            system = "aarch64-linux";
            overlay-unstable = final: prev: {
              unstable = import nixpkgs-unstable {
                inherit system;
                config.allowUnfree = true;
              };
            };
          in
          nixpkgs.lib.nixosSystem rec {
            modules = [
              ./configuration.oci-xnzm1001-001.nix
            ];
          };

        oci-xnzm1001-002 =
          let
            system = "x86_64-linux";
            overlay-unstable = final: prev: {
              unstable = import nixpkgs-unstable {
                inherit system;
                config.allowUnfree = true;
              };
            };
          in
          nixpkgs.lib.nixosSystem rec {
            modules = [
              ./configuration.oci-xnzm1001-002.nix
            ];
          };

      };
    };
}
