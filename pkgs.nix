{ config, pkgs, ... }:
{
  environment.systemPackages = with pkgs; [
    fzf
    git
    tmux
    jq
    gcc
    gettext
    killall
    fd
    inetutils
    wget
    elvish
    coreutils-full
    findutils
    moreutils
    glibcLocales
    vifm
    ghq
    stow
    gnumake
    procps
    fish
    vim

    ninja
    meson
    pkg-config
    lua
    luajit
    # waypipe
    # wl-clipboard
    # xsel
    bkt
    entr
    diskus
    zsh
    # htop
    cmake
    go
  ];
}
