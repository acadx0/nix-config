# httpr://nixos.org/guides/nix-pills/

{ config, pkgs, options, ... }:

{
  imports =
    [
      ./hardware/root.nix
      # ./pkgs/dev.nix

      ./pkgs/sway.nix
      ./pkgs/minimal.nix
    ];

  nix.settings = {
    experimental-features = "nix-command flakes";
  };


  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 7d";
  };

  programs.adb.enable = true;
  programs.fish.enable = true;

  nixpkgs.config.allowUnfree = true;

  networking.hostName = "root";
  networking.wireless.iwd.enable = true;
  services.udev.packages = with pkgs; [ via vial ];
  # KERNEL=="hidraw*", SUBSYSTEM=="hidraw", ATTRS{serial}=="*vial:f64c2b3c*", MODE="0660", GROUP="users", TAG+="uaccess", TAG+="udev-acl"
  # services.udev.extraRules = ''
  #   KERNEL=="hidraw*", SUBSYSTEM=="hidraw", MODE="0660", GROUP="users", TAG+="uaccess", TAG+="udev-acl"
  # '';

  systemd.timers = {
    "qbittorrent-reschedule" = {
      wantedBy = [ "timers.target" ];
      enable = false;
      timerConfig = {
        OnBootSec = "3m";
        OnUnitActiveSec = "3m";
        Unit = "qbittorrent-reschedule.service";
      };
    };
    "qbittorrent-clean" = {
      wantedBy = [ "timers.target" ];
      enable = true;
      timerConfig = {
        OnBootSec = "3m";
        OnUnitActiveSec = "3m";
        Unit = "qbittorrent-clean.service";
      };
    };
  };

  systemd.services = {
    # "waiter" = {
    #   enable = true;
    #   path = [ pkgs.bash ];
    #   wantedBy = [ "multi-user.target" ];
    #   after = [ "network.target" ];
    #   serviceConfig = {
    #     Type = "notify";
    #     ExecStart = ''
    #       /home/rok/bin/waiter
    #     '';
    #   };
    # };

    # "proxy-socks5" = {
    #   enable = true;
    #   path = [ ];
    #   wantedBy = [ "multi-user.target" ];
    #   after = [ "network.target" ];
    #   serviceConfig = {
    #     Type = "simple";
    #     User = "rok";
    #     Restart = "always";
    #     ExecStart = ''
    #       /run/current-system/sw/bin/ssh -D 1337 -q -C root@oci-xnzm1001-002 "sh -c 'sleep 24h; echo 1'"
    #     '';
    #   };
    # };

    "qbittorrent-nox" = {
      enable = false;
      path = [ pkgs.qbittorrent-nox ];
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      serviceConfig = {
        Type = "exec";
        User = "rok";
        ExecStart = ''
          /run/current-system/sw/bin/qbittorrent-nox
        '';
      };
    };

    "qbittorrent-reschedule" = {
      path = [ pkgs.bash pkgs.curl pkgs.jq pkgs.findutils ];
      enable = true;
      serviceConfig = {
        Type = "oneshot";
        User = "root";
        ExecStart = "/home/rok/.bin/qbittorrent-reschedule";
      };
    };

    "qbittorrent-clean" = {
      path = [ pkgs.bash pkgs.curl pkgs.jq pkgs.findutils ];
      enable = true;
      serviceConfig = {
        Type = "oneshot";
        User = "root";
        ExecStart = "/home/rok/.bin/qbittorrent-clean";
      };
    };

    "dura" = {
      enable = true;
      path = [ pkgs.dura ];
      wantedBy = [ "multi-user.target" ];
      after = [ "network.target" ];
      serviceConfig = {
        Type = "simple";
        User = "rok";
        # why path not work?
        ExecStart = "/run/current-system/sw/bin/dura serve";
      };
    };
  };

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.initrd.availableKernelModules = [ "virtiofs" ];

  boot.supportedFilesystems = [ "nfs" ];
  services.rpcbind.enable = true;

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = false;

  systemd.network.enable = true;
  systemd.network.networks = {
    "20-wlan" = {
      matchConfig.Name = "wlan0";
      networkConfig.DHCP = "yes";
      dhcpV4Config.RouteMetric = 2;
    };
    "10-enp83s0u2" = {
      matchConfig.Name = "enp83s0u2";
      networkConfig.DHCP = "yes";
      dhcpV4Config.RouteMetric = 1;
    };
  };

  # Set your time zone.
  time.timeZone = "Asia/Seoul";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  i18n.inputMethod = {
    enabled = "fcitx5";
    fcitx5.addons = with pkgs; [
      fcitx5-mozc
      fcitx5-gtk
      fcitx5-chinese-addons
      fcitx5-configtool
      fcitx5-hangul
      fcitx5-lua
    ];
  };

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

  services.upower.enable = true;
  services.fwupd.enable = true;
  # services.qbittorrent.enable = true;
  # services.qbittorrent.port = 4321;

  # security
  security.rtkit.enable = true;
  security.sudo.extraRules = [
    {
      users = [ "rok" ];
      commands = [
        {
          command = "ALL";
          options = [ "NOPASSWD" ]; # "SETENV" # Adding the following could be a good idea
        }
      ];
    }
  ];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.rok = {
    isNormalUser = true;
    description = "rok";
    extraGroups = [ "networkmanager" "wheel" "docker" "adbusers" ];
    packages = with pkgs; [ ];
  };

  nixpkgs.overlays = [
    (import (builtins.fetchTarball {
      url = https://github.com/nix-community/neovim-nightly-overlay/archive/master.tar.gz;
    }))
    (
      let
        pinnedPkgs = import
          (pkgs.fetchFromGitHub {
            owner = "NixOS";
            repo = "nixpkgs";
            rev = "b6bbc53029a31f788ffed9ea2d459f0bb0f0fbfc";
            sha256 = "sha256-JVFoTY3rs1uDHbh0llRb1BcTNx26fGSLSiPmjojT+KY=";
          })
          { };
      in
      final: prev: {
        docker = pinnedPkgs.docker;
      }
    )
  ];
  hardware.bluetooth.enable = true;
  virtualisation.docker = {
    enable = true;
    # package = unstable.docker;
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs;
    [
      # (import ./packages/sublime-merge/default.nix)
      # (import ./packages/hello/hello.nix)
    ] ++ [
      # cloud
      pkgs.unstable.awscli2
      pkgs.unstable.azure-cli
      azure-storage-azcopy
      pkgs.unstable.oci-cli

    ] ++ [
      # cloud.k8s
      kubectl
      stern
      kubectl-images
      kubectl-node-shell
      kubectl-tree
      kubectx
      kubetail

      dive

      ko
      krew
    ] ++ [
      # laptop
      upower
    ] ++ [
      # system
      glances
      htop
      iftop
      spice-vdagent
      usbutils
    ] ++ [
      # android
      pkgs.unstable.android-tools
      pkgs.unstable.android-studio
      pkgs.unstable.android-udev-rules
      pkgs.unstable.flutter
      jdk11
    ] ++ [
      # tools
      via
      vial

      # microsoft-edge-dev
      pkgs.unstable.microsoft-edge-beta
      pkgs.unstable.firefox-devedition-unwrapped
      pkgs.unstable.gimp

      pup
      socat
      sops
      sqlite
      sshfs
      sublime-merge
      # git-cola
      gitkraken
      sshpass
      telegram-desktop
      libnotify
      lsof
    ] ++ [
      dig
      inetutils
      wget
      entr
      diskus
      pcmanfm
      zsh
      xsel

      _9pfs
      age
      aria2
      atool
      bat
      bolt
      chromium
      kanshi
      ov
      cron
      delta
      ipset
      dig
      oauth2-proxy
      direnv
      glow
      docker
      dog
      pkgs.unstable.google-chrome
      pkgs.unstable.nixpkgs-fmt
      desktop-file-utils
      entr
      pandoc

      qbittorrent-nox
      (pkgs.makeDesktopItem {
        name = "qbittorrent-nox";
        desktopName = "qbittorrent-nox";
        exec = "/run/current-system/sw/bin/qbittorrent-nox %u";
        mimeTypes = [ "x-scheme-handler/magnet" "application/x-bittorrent" ];
        # icon = "nix-snowflake";
      })

      exa
      appimage-run
      qemu

      trash-cli
      webkitgtk
      git-annex-utils
      gnuplot
      gron
      nfs-utils
      helm
      # transmission
      # transmission-remote-gtk
      hexyl
      jo
      asciinema
      just
      kitty
      texlive.combined.scheme-full

      libreoffice-qt
      lnav
      lshw
      ffmpeg_6-full
      ncdu
      # neovim
      neovim-nightly
      netcat-gnu
      nginx
      nmap
      nnn
      okular
      p7zip
      unzip
      phodav
      progress
      python3
      rakudo
      tcpdump
      termshark
      terraform
      tig
      tshark
      pkgs.unstable.typst
      virtiofsd
      watchexec
      wev
      wireshark
      pkgs.unstable.yarn
      pkgs.unstable.zathura
      zef
      pkgs.unstable.bun
      patchelf
      ttyd

      pkgs.unstable.zig

      # sway stuff
      pkgs.unstable.sway
      pkgs.unstable.alacritty # gpu accelerated terminal
      # pkgs.unstable.dbus-sway-environment
      pkgs.unstable.rofi-wayland
      pkgs.unstable.wayland
      pkgs.unstable.wdisplays
      pkgs.unstable.xdg-utils
      pkgs.unstable.waypipe
      pkgs.unstable.wl-clipboard
      pkgs.unstable.xdg-utils # for opening default programs when clicking links
      pkgs.unstable.dunst
      pkgs.unstable.glib # gsettings
      pkgs.unstable.dracula-theme # gtk theme
      pkgs.unstable.gnome3.adwaita-icon-theme # default gnome cursors
      pkgs.unstable.swaylock
      pkgs.unstable.pavucontrol
      pkgs.unstable.swayidle
      pkgs.unstable.pulseaudio
      pkgs.unstable.grim # screenshot functionality
      pkgs.unstable.slurp # screenshot functionality
      pkgs.unstable.wl-clipboard # wl-copy and wl-paste for copy/paste from stdin / stdout
      pkgs.unstable.bemenu # wayland clone of dmenu
      pkgs.unstable.mako # notification system developed by swaywm maintainer
      pkgs.unstable.wdisplays # tool to configure displays







      pkgs.unstable.syncthing
      pkgs.unstable.tailscale
      pkgs.unstable.mpv
      pkgs.unstable.mupdf
      pkgs.unstable.pueue
      pkgs.unstable.helix
      pkgs.unstable.gh

      pkgs.unstable.git
      pkgs.unstable.fzf
      pkgs.unstable.tmux

      jq
      gcc
      gettext
      killall
      pkgs.unstable.fd
      pkgs.unstable.ripgrep
      inetutils
      wget
      pkgs.unstable.elvish
      coreutils-full
      findutils
      moreutils
      glibcLocales
      pkgs.unstable.vifm
      ghq
      stow
      gnumake
      procps
      pkgs.unstable.fish
      pkgs.unstable.vim


      ninja
      meson
      pkg-config
      lua
      luajit
      # waypipe
      # wl-clipboard
      # xsel
      entr
      diskus
      zsh
      # htop
      cmake
      pkgs.unstable.bkt
      pkgs.unstable.go

    ];

  services.tailscale.enable = true;

  services.openssh.enable = true;
  services.qemuGuest.enable = true;
  services.syncthing = {
    enable = true;
    user = "rok";
    dataDir = "/home/rok"; # Default folder for new synced folders
    configDir = "/home/rok/.syncthing"; # Folder for Syncthing's settings and keys
  };

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall = {
    enable = true;
    allowedTCPPortRanges = [
      { from = 4180; to = 4180; }
    ];
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

  # systemd.user.services.pueued = {
  #   path = [ pkgs.pueue ];
  #   enable = false;
  #   serviceConfig.ExecStart = "${pkgs.pueue}/bin/pueued";
  # };

  fonts = {
    enableDefaultFonts = true;
    fonts = with pkgs; [
      ubuntu_font_family
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      liberation_ttf
      fira-code
      fira-code-symbols
      mplus-outline-fonts.githubRelease
      nerdfonts
      iosevka
      dina-font
      nanum
      proggyfonts
    ];

    fontconfig = {
      defaultFonts = {
        serif = [ "NanumGothic" "Noto Sans Mono" ];
        sansSerif = [ "NanumGothic" "Noto Sans Mono" ];
        monospace = [ "Noto Sans Mono" ];
      };
    };
  };
}
