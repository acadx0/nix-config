# https://nixos.org/guides/nix-pills/

{ config, pkgs, options, ... }:

{
  imports =
    [
      ./pkgs/dev.nix
    ];

  environment.systemPackages =
    [
    ] ++ [
      # system
      pkgs.vim
      pkgs.coreutils-full
      pkgs.pueue
      pkgs.recode
      pkgs.skhd
      pkgs.htop
      pkgs.bash
      pkgs.fzf
      pkgs.fish
      pkgs.fd
      # pkgs.oracle-instantclient

    ];

  services.skhd.enable = true;
  services.skhd.skhdConfig = ''
    :: default : /opt/homebrew/bin/yabai -m config active_window_border_color 0xFF696969

    # mac
    # defaults read com.apple.spaces
    # defaults read com.apple.desktop

    # | /opt/homebrew/bin/yabai   | bspwm   |
    # | ---     | ---     |
    # | window  | node    |
    # | space   | desktop |
    # | display | X       |

    # https://github.com/vovkasm/input-source-switcher
    escape -> :issw com.apple.keylayout.US
    # escape -> :xkbswitch -s 0
    lcmd + lctrl + lalt - 0 : open '/System/Applications/Mission Control.app';
    # lcmd + lctrl + lalt - x : open -a 'Kitty';

    # application launcher
    lcmd + lctrl + lalt - x : open '/Applications/kitty.app';
    lcmd + lctrl + lalt - c : open -a 'Firefox Developer Edition';
    # lcmd + lctrl + lalt - d : open '/System/Applications/Dictionary.app'

    # focus & and swap node in the given direction
    lcmd + lctrl + lalt - h : /opt/homebrew/bin/yabai -m window --focus west
    lcmd + lctrl + lalt - j : /opt/homebrew/bin/yabai -m window --focus south
    lcmd + lctrl + lalt - k : /opt/homebrew/bin/yabai -m window --focus north
    lcmd + lctrl + lalt - l : /opt/homebrew/bin/yabai -m window --focus east
    lcmd + lctrl + lalt + shift - h : /opt/homebrew/bin/yabai -m window --swap west
    lcmd + lctrl + lalt + shift - j : /opt/homebrew/bin/yabai -m window --swap south
    lcmd + lctrl + lalt + shift - k : /opt/homebrew/bin/yabai -m window --swap north
    lcmd + lctrl + lalt + shift - l : /opt/homebrew/bin/yabai -m window --swap east

    # focus or send to the given desktop
    # skhd --reload
    lcmd + lctrl + lalt - 1 : /opt/homebrew/bin/yabai -m space --focus 1;
    lcmd + lctrl + lalt - 2 : /opt/homebrew/bin/yabai -m space --focus 2;
    lcmd + lctrl + lalt - 3 : /opt/homebrew/bin/yabai -m space --focus 3;
    lcmd + lctrl + lalt - 4 : /opt/homebrew/bin/yabai -m space --focus 4;
    lcmd + lctrl + lalt - 5 : /opt/homebrew/bin/yabai -m space --focus 5;
    lcmd + lctrl + lalt - 6 : /opt/homebrew/bin/yabai -m space --focus 6;
    lcmd + lctrl + lalt - 7 : /opt/homebrew/bin/yabai -m space --focus 7;
    lcmd + lctrl + lalt - 8 : /opt/homebrew/bin/yabai -m space --focus 8;
    lcmd + lctrl + lalt - 9 : /opt/homebrew/bin/yabai -m space --focus 9;
    lcmd + lctrl + lalt + shift - 1 : /opt/homebrew/bin/yabai -m window --space 1; /opt/homebrew/bin/yabai -m space --focus 1;
    lcmd + lctrl + lalt + shift - 2 : /opt/homebrew/bin/yabai -m window --space 2; /opt/homebrew/bin/yabai -m space --focus 2;
    lcmd + lctrl + lalt + shift - 3 : /opt/homebrew/bin/yabai -m window --space 3; /opt/homebrew/bin/yabai -m space --focus 3;
    lcmd + lctrl + lalt + shift - 4 : /opt/homebrew/bin/yabai -m window --space 4; /opt/homebrew/bin/yabai -m space --focus 4;
    lcmd + lctrl + lalt + shift - 5 : /opt/homebrew/bin/yabai -m window --space 5; /opt/homebrew/bin/yabai -m space --focus 5;
    lcmd + lctrl + lalt + shift - 6 : /opt/homebrew/bin/yabai -m window --space 6; /opt/homebrew/bin/yabai -m space --focus 6;
    lcmd + lctrl + lalt + shift - 7 : /opt/homebrew/bin/yabai -m window --space 7; /opt/homebrew/bin/yabai -m space --focus 7;
    lcmd + lctrl + lalt + shift - 8 : /opt/homebrew/bin/yabai -m window --space 8; /opt/homebrew/bin/yabai -m space --focus 8;
    lcmd + lctrl + lalt + shift - 9 : /opt/homebrew/bin/yabai -m window --space 9; /opt/homebrew/bin/yabai -m space --focus 9;

    # focus last desktop
    # 0x33: backspace
    lcmd + lctrl + lalt - 0x33 : /opt/homebrew/bin/yabai -m space --focus recent;

    # focus & move next/previous monitor, [ ]
    lcmd + lctrl + lalt - 0x1E : /opt/homebrew/bin/yabai -m display --focus next;
    lcmd + lctrl + lalt - 0x21 : /opt/homebrew/bin/yabai -m display --focus prev;
    lcmd + lctrl + lalt + shift - 0x1E : /opt/homebrew/bin/yabai -m window --display next;
    lcmd + lctrl + lalt + shift - 0x21 : /opt/homebrew/bin/yabai -m window --display prev;

    # focus the next/previous desktop in the current monitor, ' "
    # lcmd + lctrl + lalt - 0x27 : /opt/homebrew/bin/yabai -m space --focus next || /opt/homebrew/bin/yabai -m space --focus first;
    # lcmd + lctrl + lalt - 0x29 : /opt/homebrew/bin/yabai -m space --focus prev || /opt/homebrew/bin/yabai -m space --focus last;
    lcmd + lctrl + lalt - 0x27 : fish -c "/opt/homebrew/bin/yabai.circular next";
    lcmd + lctrl + lalt - 0x29 : fish -c "/opt/homebrew/bin/yabai.circular prev";
    lcmd + lctrl + lalt + shift - 0x27 : fish -c "/opt/homebrew/bin/yabai.circular next move";
    lcmd + lctrl + lalt + shift - 0x29 : fish -c "/opt/homebrew/bin/yabai.circular prev move";

    # focus last window (limit to current desktop)
    # lcmd + lctrl + lalt - w : /opt/homebrew/bin/yabai -m window --focus last
    lcmd + lctrl + lalt - 0x30 : /opt/homebrew/bin/yabai -m window --focus recent


    # space, float / unfloat window and center on screen
    # lcmd + lctrl + lalt - 0x31 : /opt/homebrew/bin/yabai -m window --toggle float; /opt/homebrew/bin/yabai -m window --grid 50:50:1:2:48:47

    # focus last window (limit to current desktop)
    lcmd + lctrl + lalt - w : /opt/homebrew/bin/yabai -m window --focus last;
    lcmd + lctrl + lalt - 0x30 : /opt/homebrew/bin/yabai -m window --focus last;
    # lcmd + lctrl + lalt - q : fish -c "/opt/homebrew/bin/yabai.circular next"; /opt/homebrew/bin/yabai -m window --focus last;
    # lcmd + lctrl + lalt - q : kill $(/opt/homebrew/bin/yabai -m query --windows --window | /opt/homebrew/bin/jq -r .pid); /opt/homebrew/bin/yabai -m window --close;
    lcmd + lctrl + lalt - q : /opt/homebrew/bin/yabai -m window --close;

    # balance size of windows, '='
    lcmd + lctrl + lalt - 0x18 : /opt/homebrew/bin/yabai -m space --balance;

    # resize
    :: resizeMode @ : /opt/homebrew/bin/yabai -m config active_window_border_color 0xFF8B0000
    lcmd + lctrl + lalt - z ; resizeMode
    resizeMode < escape ; default
    resizeMode < h : /opt/homebrew/bin/yabai -m window --resize left:-200:0; /opt/homebrew/bin/yabai -m window --resize right:-200:0
    resizeMode < j : /opt/homebrew/bin/yabai -m window --resize bottom:0:200; /opt/homebrew/bin/yabai -m window --resize top:0:200
    resizeMode < k : /opt/homebrew/bin/yabai -m window --resize top:0:-200; /opt/homebrew/bin/yabai -m window --resize bottom:0:-200
    resizeMode < l : /opt/homebrew/bin/yabai -m window --resize right:200:0; /opt/homebrew/bin/yabai -m window --resize left:200:0

    # :: appmode @ : /opt/homebrew/bin/yabai -m config active_window_border_color 0xFF000080
    # lcmd + lctrl + lalt - x ; appmode
    # appmode < escape ; default
    # appmode < c : open /Applications/Google\ Chrome.app; skhd -k "escape"
    # appmode < a : open /Applications/Alacritty.app; skhd -k "escape"
    # appmode < k : open /Applications/KakaoTalk.app; skhd -k "escape"

    # alacritty
    # lcmd + lctrl + lalt - a : open /Applications/Alacritty.app; skhd -k "escape"
    # lcmd + lctrl + lalt - a : /Users/rok/bin/neovide; skhd -k "escape"

    # quit / restart bspwm
    # lcmd + lctrl + lalt + shift - q : brew services stop /opt/homebrew/bin/yabai
    # lcmd + lctrl + lalt + shift - r : brew services restart /opt/homebrew/bin/yabai




    # # |, sleep
    # lcmd + lctrl + lalt + shift - 0x2A : pmset displaysleepnow

    # esc, mission control
    lcmd + lctrl + lalt - escape : open -a '/System/Applications/Mission Control.app';

    # rotate tree
    lcmd + lctrl + lalt - r : /opt/homebrew/bin/yabai -m space --rotate 90;

    # lcmd + lctrl + lalt - f : /opt/homebrew/bin/yabai -m window --toggle native-fullscreen
    lcmd + lctrl + lalt - f : /opt/homebrew/bin/yabai -m window --toggle zoom-fullscreen

    # toggle window native fullscreen
    # lcmd + lctrl + lalt + shift - f : /opt/homebrew/bin/yabai -m window --toggle native-fullscreen;

    # # make floating window fill screen
    # shift + alt - up     : /opt/homebrew/bin/yabai -m window --grid 1:1:0:0:1:1

    # # make floating window fill left-half of screen
    # shift + alt - left   : /opt/homebrew/bin/yabai -m window --grid 1:2:0:0:1:1

    # # make floating window fill right-half of screen
    # shift + alt - right  : /opt/homebrew/bin/yabai -m window --grid 1:2:1:0:1:1
  '';

  services.nix-daemon.enable = true;
  nix.package = pkgs.nix;

  programs.fish.enable = true;
  programs.zsh.enable = true;
  system = {
    stateVersion = 4;

    defaults = {
      LaunchServices = {
        LSQuarantine = false;
      };

      NSGlobalDomain = {
        AppleShowAllExtensions = true;
        ApplePressAndHoldEnabled = false;

        # 120, 90, 60, 30, 12, 6, 2
        KeyRepeat = 2;

        # 120, 94, 68, 35, 25, 15
        InitialKeyRepeat = 15;

        "com.apple.mouse.tapBehavior" = 1;
        "com.apple.sound.beep.volume" = 0.0;
        "com.apple.sound.beep.feedback" = 0;
      };

      dock = {
        autohide = true;
        show-recents = false;
        launchanim = true;
        orientation = "bottom";
        tilesize = 48;
      };

      finder = {
        _FXShowPosixPathInTitle = true;
      };

      trackpad = {
        Clicking = true;
        TrackpadThreeFingerDrag = true;
      };

    };

    keyboard = {
      enableKeyMapping = true;
      remapCapsLockToControl = true;
    };

  };

  launchd.daemons.enableSSH = {
    command = ''
      /usr/sbin/systemsetup -setremotelogin on
    '';
    serviceConfig.RunAtLoad = true;
    serviceConfig.KeepAlive = false;
    serviceConfig.StandardErrorPath = "/Users/kyungrok.chung/tmp/enableSSH.stdout.log";
    serviceConfig.StandardOutPath = "/Users/kyungrok.chung/tmp/enableSSH.stderr.log";
  };

  launchd.daemons.caffeinate = {
    # script = ''
    #   sudo /usr/bin/caffeinate -d
    # '';
    command = ''
        /usr/bin/caffeinate -d
    '';
    serviceConfig.RunAtLoad = true;
    serviceConfig.KeepAlive = false;
  };

  # launchd.daemons.helloworld = {
  #   command = "${pkgs.bash} -c 'echo hello >> /Users/kyungrok.chung/tmp/debug'";
  #   serviceConfig.RunAtLoad = true;
  #   # serviceConfig.StandardErrorPath = "/var/log/prometheus-node-exporter.log";
  #   # serviceConfig.StandardOutPath = "/var/log/prometheus-node-exporter.log";
  # };

}
